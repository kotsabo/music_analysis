#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Created on Tue Jan 30 22:57:00 2018

@author: kotsabo
"""

import sys

#append the path where you stored the utilies.py file
sys.path.append('/Users/kotsabo/Desktop/MLP_CW3/Code/')

import utilities
import numpy as np
import sklearn as skl
import sklearn.preprocessing
import tensorflow as tf

tf.logging.set_verbosity(tf.logging.INFO)

#change the path to the directory you have saved the project
#you need to have under the same directory the tracks.csv, genres.csv, features.csv files
#from the metadata of the FMA Dataset
path = '/Users/kotsabo/Desktop/MLP_CW3/Data/'

# Load metadata and features.
tracks = utilities.load_data(path + 'tracks.csv')
genres = utilities.load_data(path + 'genres.csv')
features = utilities.load_data(path + 'features.csv')

medium = tracks['set', 'subset'] <= 'medium'

train = tracks['set', 'split'] == 'training'
val = tracks['set', 'split'] == 'validation'
test = tracks['set', 'split'] == 'test'

y_train = tracks.loc[medium & train, ('track', 'genre_top')]
y_val = tracks.loc[medium & val, ('track', 'genre_top')]
y_test = tracks.loc[medium & test, ('track', 'genre_top')]

enc = sklearn.preprocessing.LabelEncoder()
y_train = enc.fit_transform(y_train)
y_val = enc.fit_transform(y_val)
y_test = enc.transform(y_test)

X_train = features.loc[medium & train, ['mfcc', 'spectral_contrast']]
X_val = features.loc[medium & val, ['mfcc', 'spectral_contrast']]
X_test = features.loc[medium & test, ['mfcc', 'spectral_contrast']]

print('{} training examples, {} validation examples'.format(y_train.size, y_val.size))
print('{} features, {} classes'.format(X_train.shape[1], np.unique(y_train).size))

# Be sure training samples are shuffled.
X_train, y_train = skl.utils.shuffle(X_train, y_train, random_state = 42)

# Standardize features by removing the mean and scaling to unit variance.
scaler = skl.preprocessing.StandardScaler(copy = False)
scaler.fit_transform(X_train)
scaler.transform(X_val)
scaler.transform(X_test)

X_final_train = dict()
for i in range(0, X_train.values.shape[1]):
    key = 'x{}'.format(i)
    X_final_train[key] = X_train.values[:, i]

X_final_val = dict()
for i in range(0, X_val.values.shape[1]):
    key = 'x{}'.format(i)
    X_final_val[key] = X_val.values[:, i]

X_final_test = dict()
for i in range(0, X_test.values.shape[1]):
    key = 'x{}'.format(i)
    X_final_test[key] = X_test.values[:, i]

# Feature columns describe how to use the input.
my_feature_columns = []
for key in X_final_train.keys():
    my_feature_columns.append(tf.feature_column.numeric_column(key = key))

step_size = 100
my_checkpointing_config = tf.estimator.RunConfig(
    save_checkpoints_steps = step_size,
    keep_checkpoint_max = 1000
)

def train_input_fn(features, labels, batch_size):
    """An input function for training"""
    # Convert the inputs to a Dataset.
    dataset = tf.data.Dataset.from_tensor_slices((dict(features), labels))
    
    # Shuffle, repeat, and batch the examples.
    dataset = dataset.shuffle(500).repeat().batch(batch_size)
    
    # Return the read end of the pipeline.
    return dataset.make_one_shot_iterator().get_next()

def eval_input_fn(features, labels, batch_size):
    """An input function for evaluation or prediction"""
    features = dict(features)
    if labels is None:
        # No labels, use only features.
        inputs = features
    else:
        inputs = (features, labels)
    
    # Convert the inputs to a Dataset.
    dataset = tf.data.Dataset.from_tensor_slices(inputs)

    # Batch the examples
    assert batch_size is not None, "batch_size must not be None"
    dataset = dataset.batch(batch_size)
    
    # Return the read end of the pipeline.
    return dataset.make_one_shot_iterator().get_next()

# the path you want the checkpoints to be saved
checkpoints_path = '/Users/kotsabo/Desktop/MLP_CW3/Results/Optimizers/Adam/0.00001/'

# Build n hidden layer DNN with N0,...,Nn hidden units respectively.
classifier = tf.estimator.DNNClassifier(
    feature_columns = my_feature_columns,
    hidden_units = [100, 100], # n hidden layers of N0,...,Nn nodes each.
    #optimizer = tf.train.AdamOptimizer(learning_rate = 0.00001), #default Adagrad
    #activation_fn = tf.nn.selu, # if None default relu
    n_classes = 16, # The model must choose between 16 classes.
    #dropout = 0.1, #default None
    model_dir = checkpoints_path,
    config = my_checkpointing_config
)

num_steps = 1501

# Train the Model.
trainer = classifier.train(
   input_fn = lambda: train_input_fn(X_final_train, y_train, batch_size = 50),
   steps = num_steps
)

# Evaluate the model.
for i in range(1, num_steps + 1, step_size):
    model_path = checkpoints_path + 'model.ckpt-{}'.format(i)

    train_result = classifier.evaluate(
        input_fn = lambda: eval_input_fn(X_final_train, y_train, y_train.size),
        checkpoint_path = model_path,
        name = 'train'
    )

    eval_result = classifier.evaluate(
        input_fn = lambda: eval_input_fn(X_final_val, y_val, y_val.size),
        checkpoint_path = model_path,
        name = 'val'
    )


