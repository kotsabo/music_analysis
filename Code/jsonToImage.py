#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 19:30:02 2018

@author: kotsabo
"""

import sys
import json
import csv
import numpy as np
import matplotlib.pyplot as plt

if len(sys.argv) < 5:
    raise AssertionError("You need to pass: \nfirst argument the training \
                         jsonfile \nsecond argument the validation jsonfile \
                         \nthird argument the imagename\nfourth argument ylabel")
else:
    training_jsonfilepath = sys.argv[1]
    validation_jsonfilepath = sys.argv[2]
    imagename = sys.argv[3]
    ylabel = sys.argv[4]
    if imagename[-4:] != '.pdf' :
        raise AssertionError("You are an idiot!! \nWe want the image in pdf format...")

train_data = json.load(open(training_jsonfilepath))
val_data = json.load(open(validation_jsonfilepath))

data_train_array = np.array(train_data)
data_val_array = np.array(val_data)

X_train = data_train_array[:,1]
Y_train = data_train_array[:,2]

X_val = data_val_array[:,1]
Y_val = data_val_array[:,2]

plt.figure()
plt.plot(X_train, Y_train, '-b', label = 'training set')
plt.plot(X_val, Y_val, '-r', label = 'validation set')
plt.ylabel(ylabel)
plt.xlabel('Steps')
plt.legend(loc = 'upper left')
plt.savefig(imagename)
plt.close()

"""
csv_path = '/Users/kotsabo/Desktop/' + ylabel + '.csv'

with open(csv_path, 'w') as myfile:
    wr = csv.writer(myfile, quoting = csv.QUOTE_ALL)
    wr.writerow(X_train)
    wr.writerow(Y_train)
    wr.writerow(X_val)
    wr.writerow(Y_val)
"""
