#!/bin/bash

i=0
while read line
do
    array[ $i ]="$line"        
    (( i++ ))
done < <(find /Users/kotsabo/Desktop/MLP_CW3/Results/Dropout/ -name *.json)

echo ${#array[@]}
#echo ${array[2]}.pdf

for i in $(seq 0 4 ${#array[@]})
do 
    python jsonToImage.py ${array[i]} ${array[i+1]} ${array[i]}.pdf Accuracy
    python jsonToImage.py ${array[i+3]} ${array[i+2]} ${array[i+2]}.pdf AverageLoss
    #echo ${array[i]}
    #echo ${array[i+1]}
    #echo ${array[i+2]}
    #echo ${array[i+3]}
done
